import logo from "./winamax.png";
import "./App.css";

import React, { Component } from "react";
import io from "socket.io-client";

//Encryption for CID
const multihashing = require("multihashing-async");
const bytes = new TextEncoder("utf8").encode("Winamax");
const CID = require("cids");

const socket = io("http://localhost:3002", {
  cors: {
    origin: "*",
  },
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bet: "",
      count: 0,
      betProceed: 0,
      date: "",
      timeToProceed: 0,
      isProceeding: true,
      socketId: "",
    };
    this.sendBet = this.sendBet.bind(this);
    this.chronometer = this.chronometer.bind(this);
    socket.on("bet saved", (data) => {
      this.setState({ bet: JSON.stringify(data) });
    });
    socket.on("bet proceed", (data) => {
      this.setState((prevState) => ({
        betProceed: prevState.betProceed + 1,
      }));
    });
    socket.on("connect", () => {
      this.setState({ socketId: socket.id });
    });
  }

  async sendBet() {
    this.setState({ betProceed: 0 });
    const hash = await multihashing(bytes, "sha2-256");
    let count = document.getElementById("bet-value").value;
    document.getElementById("bet-value").value = "";
    this.setState({ count: count });
    let cid = new CID(1, "raw", hash, "base64");
    const bet = {
      command: "enqueue",
      count,
      mid: cid.toString(),
    };
    this.setState({ isProceeding: true });
    this.setState({ date: Date.now() });
    this.chronometer();
    console.log("id de la socket client front : ", socket.id);
    socket.emit("pending client bet", bet);
  }

  chronometer() {
    this.setState(() => ({
      timeToProceed: this.mesureTime(),
    }));
    setInterval(() => {
      if (this.state.betProceed < this.state.count) {
        this.chronometer();
      }
    }, 10);
  }

  mesureTime() {
    let now = Date.now();
    let date = now - this.state.date;
    return date;
  }

  render() {
    let timeProceeding = this.state.timeToProceed / 1000;
    return (
      <div className="App">
        <header className="App-header">
          <div className="title-client">
            <img src={logo} className="App-logo" alt="logo" />
            <p>Test technique Winamax</p>
          </div>
          <div className="form-client">
            <input
              type="number"
              id="bet-value"
              className="input-client"
              placeholder="Enter number of bets to send"
            ></input>
            <button className="send">
              <a
                className="App-link"
                onClick={this.sendBet}
                target="_blank"
                rel="noopener noreferrer"
              >
                SEND
              </a>
            </button>
          </div>

          <div className="results">
            <div className="numbers">
              Numbers of bets processed :{" "}
              <span className="number-proceeding" value={this.state.betProceed}>
                {this.state.betProceed}
              </span>
              /
              <span className="number-total" value={this.state.count}>
                {this.state.count}
              </span>
            </div>
            <div className="status">Time to process : {timeProceeding}s</div>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
