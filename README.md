# 1/ Installation

Dans chaque sous répertoire du projet (api, client, redis et worker), installez les dépendances :

```
npm install
```

# 2/ Configuration

Par défaut, les projets tournent sur ports attirés :

api : port 3002 <br>
redis : port 3003 <br>
worker : port 3004 <br>
client : port 3000 <br> <br>

Si vous souhaitez en changer :

## A/ Pour les microservices api, redis et worker :

Créez un .env à la racine du projet, et ajoutez :

```
PORT= [port]
```

## B/ Pour le microservice client :

Ouvrez le fichier package.json, et modifiez comme suit le script :

```
"scripts": {
  "start": "PORT=[port] react-scripts start",
  ...
}

```

# 3/ Lancement

Dans chaque sous répertoire du projet (api, client, redis et worker), lancez le script de démarrage :

```
npm start
```
