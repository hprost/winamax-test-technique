require("dotenv").config();
console.log(console.log(process.env));
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

/***********************************************Redis pub/sub*********************************************************** */

const redis = require("redis");
const client = redis.createClient();
const { Worker, isMainThread } = require("worker_threads");

(async () => {
  await client.connect();
})();

const subscriber = client.duplicate();

(async () => {
  await subscriber.connect();
})();

subscriber.on("connect", () =>
  console.log("Redis Client Worker Subscriber Connected")
);
subscriber.on("error", (err) =>
  console.log("Redis Client Worker Subscriber Connection Error", err)
);

//Déclenchement à la réception du message de la BDD Redis
subscriber.subscribe("redis bet stored", async (message) => {
  // récupération de tous les paris et initialisation de variables --> on lance le processus de traitement launchHell()
  let bets = await client.lRange("bets", 0, -1);
  let betsLength = bets.length;
  let launchCount = 0;

  async function createWorkers() {
    // on vérifie que le nombre de workers utilisés est inférieur au nombre de CPU du système
    if (app.locals.workers < app.locals.cpus) {
      // si c'est bon, on récupère le premier pari (pop)
      let bet = await client.lPop("bets");
      //on incrémente le compteur de worker au travail
      app.locals.workers++;
      // on créé le worker
      const worker = new Worker("./service/worker.js", {
        workerData: { message: message, bet: bet },
      });
      // lorsque le worker a fini son traitement et envoit son message
      worker.on("message", (message) => {
        if ((message = "bet processed")) {
          launchCount--;
          // on le détruit
          worker.terminate();
        }
      });
    }
  }

  async function launchHell() {
    // S'il reste des paris à traiter...
    if (betsLength > 0) {
      // ... et que le traitement n'a pas été lancé autant pu plus de fois que le nombre de CPUs du système
      if (launchCount < app.locals.cpus) {
        launchCount++;
        // on créé un worker
        await createWorkers();
        app.locals.workers--;
        betsLength--;
        launchHell();
      } else {
        //sinon, on attend pour ne pas saturer la mémoire
        await new Promise((resolve) => setTimeout(resolve, 10));
        launchHell();
      }
    } else {
      console.log("Bets all processed");
    }
  }

  launchHell();
});

module.exports = app;
