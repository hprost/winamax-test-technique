const { parentPort, workerData } = require("worker_threads");
const redis = require("redis");
const client = redis.createClient();
const axios = require("axios");
//const app = require("../app");

(async () => {
  await client.connect();
})();

const bet = workerData.bet;
const min = 300;
const max = 500;
const delay = Math.floor(Math.random() * (max - min + 1) + min);

function setDelay() {
  const delay = Math.floor(Math.random() * (max - min + 1) + min);
  console.log("DELAIS CREE :", delay, "ms");
  return delay;
}

async function postDatas(bet) {
  let cid = bet.split(":")[0];
  let mid = bet.split(":")[1];
  let idx = bet.split(":")[2];
  try {
    const response = await axios.post("http://127.0.0.1:3002/proceed", {
      command: "processed",
      idx,
      cid,
      mid,
    });
    parentPort.postMessage("bet processed");
    return true;
  } catch (error) {
    console.error(error);
    return true;
  }
}

setTimeout(() => {
  postDatas(bet);
}, setDelay());
