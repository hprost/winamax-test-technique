var express = require("express");
const router = express.Router();
const redis = require("redis");
const client = redis.createClient();

client.on("error", (err) => console.log("Erreur : " + err));
client.on("connect", () => console.log("Connecté à Redis"));

/* GET home page. */
router.get("/", function (req, res, next) {
  client.get("bets", (err, data) => {
    if (err) return res.status(500).send(err);
    if (data != null) {
      const bets = JSON.parse(data);
      bets.forEach((bet) => client.rpush("bets_queue", JSON.stringify(bet)));
      console.log(res.send(bets));
      return res.send(bets);
    } else {
      res.send("no bets");
    }
  });
});

module.exports = router;
