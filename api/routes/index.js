var express = require("express");
var router = express.Router();

router.post("/proceed", function (req, res, next) {
  let betProcessed = {
    command: "processed",
    idx: req.body.idx,
    cid: req.body.cid,
    mid: req.body.mid,
  };
  try {
    req.app.locals.io.to(req.body.cid).emit("bet proceed", betProcessed);
    res.send("bet gotten");
  } catch (err) {
    res.send(err.message);
  }
});

module.exports = router;
