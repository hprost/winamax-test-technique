function generateBets(datas, id) {
  console.log("datas à traiter : ", datas);
  let bets = [];
  for (let i = 0; i < datas.count; i++) {
    const bet = `${id}:${datas.mid}:${i}`;
    bets.push(bet);
  }
  return bets;
}

module.exports = generateBets;
