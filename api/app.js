require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var http = require("http");

const redis = require("redis");
const bets = require("./services/bet.js");

var indexRouter = require("./routes/index");

var app = express();

/**
 * Get port from environment and store in Express.
 */

var port = process.env.PORT || "3002";
app.set("port", port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

var cors = require("cors");
app.use(cors());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use("/", indexRouter);
//app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

/*************************************************Redis pub/sub********************************************************** */

const redisClient = redis.createClient();
const publisher = redisClient.duplicate();

(async () => {
  await publisher.connect();
})();

publisher.on("connect", () => console.log("Redis Client Publisher Connected"));
publisher.on("error", (err) =>
  console.log("Redis Client Publisher Connection Error", err)
);

/************************************************Websocket*********************************************************** */

const { Server } = require("socket.io");
const io = new Server({
  cors: {
    origin: "*",
  },
});

app.locals.io = io;

io.on("connection", (socket) => {
  console.log("a user connected");

  socket.on("pending client bet", (message) => {
    socket.emit("bet saved", message);
    publisher.publish("bets to send", JSON.stringify(bets(message, socket.id)));
  });

  // Gestionnaire d'événements pour la déconnexion du client
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

const serv = app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

io.listen(serv);

module.exports = { app, server, io };
